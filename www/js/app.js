// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'ngAnimate', 'ngCordova', 'ngSanitize'])
  
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    navigator.splashscreen.hide();
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('tabs', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })
    .state('tabs.hot', {
      url: "/hot",
      views: {
        'hot-tab': {
          templateUrl: "templates/hot.html",
          controller: 'HotTabCtrl'
        }
      }
    })
    .state('tabs.trending', {
      url: "/trending",
      views: {
        'trending-tab': {
          templateUrl: "templates/trending.html",
          controller: 'TrendingTabCtrl'
        }
      }
    })
    .state('tabs.fresh', {
      url: "/fresh",
      views: {
        'fresh-tab': {
          templateUrl: "templates/fresh.html",
          controller: 'FreshTabCtrl'
        }
      }
    })
    .state('tabs.fresh-item', {
      url: "/fresh-item/:item_id",
      views: {
        'fresh-tab': {
          templateUrl: "templates/item.html",
          controller: 'ItemCtrl'
        }
      }
    })
    .state('tabs.trending-item', {
      url: "/trending-item/:item_id",
      views: {
        'trending-tab': {
          templateUrl: "templates/item.html",
          controller: 'ItemCtrl'
        }
      }
    })
    .state('tabs.hot-item', {
      url: "/hot-item/:item_id",
      views: {
        'hot-tab': {
          templateUrl: "templates/item.html",
          controller: 'ItemCtrl'
        }
      }
    })
    ;

   $urlRouterProvider.otherwise("tab/hot");

})

.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http*://img-9gag-fun.9cache.com/**'
  ])
})

.config(function($ionicConfigProvider) {
  //$ionicConfigProvider.views.transition('none');
  $ionicConfigProvider.scrolling.jsScrolling(false);
  $ionicConfigProvider.views.maxCache(1);
})

.controller('HotTabCtrl', function ($scope, $http, $ionicTabsDelegate) {
    $scope.listCanSwipe = true;
    $scope.loading = true;

    $http
      .get('http://infinigag.k3min.eu/hot')
      .success(function(data){
        console.log("success hot images loaded");
        return $scope.images = data.data;
    })
      .finally(function() {
        $scope.loading = false;
    });

    $scope.doRefresh = function() {
      $http.get('http://infinigag.k3min.eu/hot')
       .success(function(data) {
         $scope.images = data.data;
       })
       .finally(function() {
         // Stop the ion-refresher from spinning
         $scope.$broadcast('scroll.refreshComplete');
       });
    };

    $scope.goForward = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1) {
            $ionicTabsDelegate.select(selected + 1);
        }
    }

    $scope.goBack = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1 && selected != 0) {
            $ionicTabsDelegate.select(selected - 1);
        }
    }
})

.controller('TrendingTabCtrl', function ($scope, $http, $ionicTabsDelegate) {
    $scope.listCanSwipe = true;
    $scope.loading = true;

    $http
      .get('http://infinigag.k3min.eu/trending')
      .success(function(data){
        console.log("success trending images loaded");
        return $scope.images = data.data;
    })
      .finally(function() {
        $scope.loading = false;
    });

    $scope.doRefresh = function() {
      $http.get('http://infinigag.k3min.eu/trending')
       .success(function(data) {
         $scope.images = data.data;
       })
       .finally(function() {
         // Stop the ion-refresher from spinning
         $scope.$broadcast('scroll.refreshComplete');
       });
    };

    $scope.goForward = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1) {
            $ionicTabsDelegate.select(selected + 1);
        }
    }

    $scope.goBack = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1 && selected != 0) {
            $ionicTabsDelegate.select(selected - 1);
        }
    }
})

.controller('FreshTabCtrl', function ($scope, $http, $ionicTabsDelegate) {
    $scope.listCanSwipe = true;
    $scope.loading = true;

    $http
      .get('http://infinigag.k3min.eu/fresh')
      .success(function(data){
      console.log("success fresh images loaded");
      return $scope.images = data.data;
    })
      .finally(function() {
        $scope.loading = false;
    });

    $scope.doRefresh = function() {
      $http.get('http://infinigag.k3min.eu/fresh')
       .success(function(data) {
         $scope.images = data.data;
       })
       .finally(function() {
         $scope.$broadcast('scroll.refreshComplete');
       });
    };

    $scope.goForward = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1) {
            $ionicTabsDelegate.select(selected + 1);
        }
    }

    $scope.goBack = function () {
        var selected = $ionicTabsDelegate.selectedIndex();
        if (selected != -1 && selected != 0) {
            $ionicTabsDelegate.select(selected - 1);
        }
    }
})

.controller('ItemCtrl', function ($scope, $http, $stateParams, $ionicHistory, $window, $cordovaSocialSharing, $sce) {
    var item_id = $stateParams.item_id;
    $scope.loading = true;

    $http.get('http://infinigag.k3min.eu/gag/' + item_id)
      .success(function(data){
        return $scope.data = data;
    })
      .finally(function() {
        $scope.loading = false;
    });

    $http.get('http://infinigag.k3min.eu/comments/' + item_id + '?order=score')
      .success(function(data){
        return $scope.comments = data;
    })
      .finally(function() {
        $scope.loading = false;
    });

    $scope.goBack = function() {
      $ionicHistory.goBack();
    };

    $scope.openInBrowser = function() {
      $window.open($scope.data.link, "_blank");
    };

    $scope.showSharingOptions = function () {
      var message = $scope.data.caption + ' @via 9GAG Pro';
      var subject = $scope.data.caption;
      var file = '';
      var link = $scope.data.link;

      $cordovaSocialSharing
        .share(message, subject, file, link) 
        .then(function(result) {
          // Success!
        }, function(err) {
          // An error occured. Show a message to the user
      });

    };

    $scope.deliberatelyTrustDangerousSnippet = function(text) {
      return $sce.trustAsHtml(text);
    };
})

;
